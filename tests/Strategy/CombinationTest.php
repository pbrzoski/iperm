<?php

declare(strict_types=1);

use Generator\Strategy\Combination;
use PHPUnit\Framework\TestCase;

/**
 * Class CombinationTest
 */
final class CombinationTest extends TestCase
{
    /**
     * testCanBeCreated
     *
     * @return void
     */
    public function testCanBeCreated(): void
    {
        $this->assertInstanceOf(
            Combination::class,
            new Combination()
        );
    }

    /**
     * testCollectionSize
     *
     * @return void
     */
    public function testCollectionSize(): void
    {
        $combination = new Combination();

        $this->assertCount(3, $combination->generate(['A', 'B', 'C'], 1));
        $this->assertCount(6, $combination->generate(['A', 'B', 'C'], 2));
        $this->assertCount(7, $combination->generate(['A', 'B', 'C'], 3));
        $this->assertCount(7, $combination->generate(['A', 'B', 'C'], 4));
    }

    /**
     * testCorrectnessAllSetsOfCombinations
     *
     * @return void
     */
    public function testCorrectnessAllSetsOfCombinations(): void
    {
        $strategy = new Combination();
        $collection = $strategy->generate(['A', 'B', 'C'], 1);
        $this->assertEquals('A', implode('', $collection[0]));
        $this->assertEquals('B', implode('', $collection[1]));
        $this->assertEquals('C', implode('', $collection[2]));

        $collection = $strategy->generate(['A', 'B', 'C'], 3);
        $this->assertEquals('A', implode('', $collection[0]));
        $this->assertEquals('B', implode('', $collection[1]));
        $this->assertEquals('C', implode('', $collection[2]));
        $this->assertEquals('AB', implode('', $collection[3]));
        $this->assertEquals('AC', implode('', $collection[4]));
        $this->assertEquals('BC', implode('', $collection[5]));
        $this->assertEquals('ABC', implode('', $collection[6]));
    }
}
