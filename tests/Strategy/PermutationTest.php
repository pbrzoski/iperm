<?php

declare(strict_types=1);

use Generator\Strategy\Permutation;
use PHPUnit\Framework\TestCase;

/**
 * Class PermutationTest
 */
final class PermutationTest extends TestCase
{
    /**
     * testCanBeCreated
     *
     * @return void
     */
    public function testCanBeCreated(): void
    {
        $this->assertInstanceOf(
            Permutation::class,
            new Permutation()
        );
    }

    /**
     * testPermutation
     *
     * @return void
     */
    public function testPermutation(): void
    {
        $strategy = new Permutation();

        $items = $strategy->permutation(['A', 'B', 'C'], 10);

        $collection = [];
        foreach ($items as $item) {
            $collection[] = implode('', $item);
        }

        $this->assertEquals('ABC', $collection[0]);
        $this->assertEquals('BAC', $collection[1]);
        $this->assertEquals('BCA', $collection[2]);
        $this->assertEquals('ACB', $collection[3]);
        $this->assertEquals('CAB', $collection[4]);
        $this->assertEquals('CBA', $collection[5]);
    }

    /**
     * testItemsAreNotLessThan3
     *
     * @return void
     */
    public function testItemsAreNotLessThan3(): void
    {
        $strategy = new Permutation();

        $collection = $strategy->generate(['A', 'B'], 5);

        $this->assertEmpty($collection);
    }

    /**
     * testItemsAreNotGreatThan25
     *
     * @return void
     */
    public function testItemsAreNotGreatThan25(): void
    {
        $strategy = new Permutation();

        $collection = $strategy->generate(
            [
                'A123456789101112131415A123456789101112131415',
                'B123456789101112131415A123456789101112131415',
            ],
            5
        );

        $this->assertEmpty($collection);
    }

    /**
     * testCorrectnessAllSetsOfPermutations
     *
     * @return void
     */
    public function testCorrectnessAllSetsOfPermutations(): void
    {
        $strategy = new Permutation();

        $collection = $strategy->generate(['john', '99'], 14);
        $this->assertEquals('john', $collection[0]);
        $this->assertEquals('john_99', $collection[1]);
        $this->assertEquals('99_john', $collection[2]);
    }
}
