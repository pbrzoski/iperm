<?php

declare(strict_types=1);

use Generator\Client;
use Generator\Strategy\Strategy;
use PHPUnit\Framework\TestCase;

/**
 * Class ClientTest
 */
final class ClientTest extends TestCase
{
    /**
     * testCanBeCreated
     *
     * @return void
     */
    public function testCanBeCreated(): void
    {
        $this->assertInstanceOf(
            Client::class,
            new Client($this->createMock(Strategy::class))
        );
    }

    /**
     * testCompleteCollection
     *
     * @return void
     */
    public function testCompleteCollection(): void
    {
        $client = new Client($this->createMock(Strategy::class));

        $this->assertCount(0, $client->completeCollection([], 0));
        $this->assertCount(1, $client->completeCollection([], 1));
        $this->assertCount(2, $client->completeCollection(['test1', 'test2'], 1));
        $this->assertCount(3, $client->completeCollection(['test1', 'test2'], 3));
        $this->assertCount(5, $client->completeCollection(['test1', 'test2', 'test3'], 5));
    }

    /**
     * testClearInput
     *
     * @return void
     */
    public function testClearInput(): void
    {
        $client = new Client($this->createMock(Strategy::class));

        $dataset = [
          'test',
          '_test2',
          'test3_test',
          'te$"/}s^t_4',
          'test_@test.com5',
          '5738383',
        ];

        $output = $client->clearInput($dataset);

        $this->assertEquals('test', $output[0]);
        $this->assertEquals('test2', $output[1]);
        $this->assertEquals('test3_test', $output[2]);
        $this->assertEquals('test_4', $output[3]);
        $this->assertEquals('test', $output[4]);
        $this->assertEquals('5738383', $output[5]);
    }
}
