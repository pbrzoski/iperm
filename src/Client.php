<?php

declare(strict_types=1);

namespace Generator;

use Generator\Strategy\Strategy;

/**
 * Class Generator
 * @package Generator
 */
class Client
{
    /**
     * @var Strategy
     */
    private $strategy;

    /**
     * Client constructor.
     *
     * @param Strategy $strategy strategy
     */
    public function __construct(Strategy $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * Generate
     *
     * @param array $elements       elements
     * @param int   $collectionSize collectionSize
     * @return array
     */
    public function generate(array $elements = [], int $collectionSize = 5): array
    {
        $collection = $this->strategy->generate(
            $this->clearInput($elements),
            $collectionSize
        );

        return $this->completeCollection($collection, $collectionSize);
    }

    /**
     * Complete collection
     *
     * @param array $collection   collection
     * @param int   $exceptedSize excepted size
     * @return array
     */
    public function completeCollection(array $collection, int $exceptedSize): array
    {
        $numberOfMissingElements = $exceptedSize - count($collection);

        if ($numberOfMissingElements < 0) {
            return $collection;
        }

        for ($i = 1; $i <= $numberOfMissingElements; $i++) {
            $collection[] = mt_rand(1000000000, 9999999999);
        }

        return $collection;
    }

    /**
     * Clear input
     *
     * @param array $elements elements
     * @return array
     */
    public function clearInput(array $elements = []): array
    {
        $collection = array_map(function ($element) {

            if (filter_var($element, FILTER_VALIDATE_EMAIL)) {
                $element = substr($element, 0, (int)strpos($element, '@'));
            }

            return trim(
                preg_replace("/[^a-z0-9_]/", "", strtolower($element)),
                '_'
            );
        }, $elements);

        return $collection;
    }
}
