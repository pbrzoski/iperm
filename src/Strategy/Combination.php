<?php

declare(strict_types=1);

namespace Generator\Strategy;

/**
 * Class Combination
 * @package Generator\Strategy
 */
class Combination implements Strategy
{
    /**
     * Combine elements
     *
     * @param array $elements elements
     * @param int   $maxSize  max size
     * @return \Generator
     */
    public function combination(array $elements, int $maxSize)
    {
        $remainingLength = count($elements) - $maxSize + 1;
        for ($i = 0; $i < $remainingLength; ++$i) {
            if (1 === $maxSize) {
                yield [$elements[$i]];
            } else {
                $remaining = array_slice($elements, $i + 1);
                foreach ($this->combination($remaining, $maxSize - 1) as $combination) {
                    array_unshift($combination, $elements[$i]);
                    yield $combination;
                }
            }
        }
    }

    /**
     * Generate
     *
     * @param array $elements elements
     * @param int   $maxSize  max size
     * @return array
     */
    public function generate(array $elements, int $maxSize): array
    {
        $combinations = [];

        for ($i = 1; $i <= $maxSize; $i++) {
            foreach ($this->combination($elements, $i) as $combination) {
                $combinations[] = $combination;
            }
        }

        return $combinations;
    }
}
