<?php

declare(strict_types=1);

namespace Generator\Strategy;

/**
 * Interface Strategy
 * @package Generator\Strategy
 */
interface Strategy
{
    /**
     * @param array $elements elements
     * @param int   $maxSize  max size
     * @return array
     */
    public function generate(array $elements, int $maxSize): array;
}
