<?php

declare(strict_types=1);

namespace Generator\Strategy;

/**
 * Class Permutation
 * @package Generator\Strategy
 */
class Permutation implements Strategy
{
    const MIN_SIZE = 3;
    const MAX_SIZE = 25;

    /**
     * @var Combination
     */
    protected $combination;

    /**
     * Permutation constructor.
     */
    public function __construct()
    {
        $this->combination = new Combination();
    }

    /**
     * Permute
     *
     * @param array $elements elements
     * @return \Generator
     */
    public function permutation(array $elements)
    {
        if (count($elements) <= 1) {
            yield $elements;
        } else {
            foreach ($this->permutation(array_slice($elements, 1)) as $permutation) {
                foreach (range(0, count($elements) - 1) as $i) {
                    yield array_merge(
                        array_slice($permutation, 0, $i),
                        [$elements[0]],
                        array_slice($permutation, $i)
                    );
                }
            }
        }
    }

    /**
     * Generate
     *
     * @param array $elements elements
     * @param int   $maxSize  max size
     * @return array
     */
    public function generate(array $elements, int $maxSize): array
    {
        $permutations = [];

        foreach ($this->combination->generate($elements, count($elements)) as $combination) {
            foreach ($this->permutation($combination) as $permutation) {
                $item = implode('_', $permutation);
                $itemLength = strlen($item);
                if ($itemLength > self::MIN_SIZE && $itemLength < self::MAX_SIZE) {
                    $permutations[] = $item;
                }
                if (count($permutations) >= $maxSize) {
                    return $permutations;
                }
            }
        }

        return $permutations;
    }
}
